# cinema-api

This is backend coding challenge for software engineer position at Fourthwall.

Program was compiled with OpenJDK 16.0.1. Target java is 8.

**What is implemented:**
- you can check all API resources in `cinema.http` (IntelliJ's http client format)
- there are three main packages: 
    - app - "domain" related stuff, 
    - "framework" - a.k.a. "infrastructure" = things related to spring, OMDB, DB etc.
    - "web" - http access layer

**What is missing:**
- transaction manager - I used `JdbcTemplate` to write simple queries, due to that `@Transactional` won't work out of the box
- transactions in tests - because of point above, transactions are not supported in tests for cleaning up purposes, I went with naive solution which is
```
void tearDown() { jdbcTemplate.execute('cleanup query'); }
```
- serialization/deserialization could be done better - for example dedicated serializer for `MovieId` value object
- unfortunately I don't have enough time for OpenAPI/Swagger spec for API resources (that's why I attached `cinema.http`) :shrug:
- logs
- metadata caching
- users management - there is hardcoded in memory implementation provided by spring security, obviously users should be in DB.
- JOOQ instead manual queries to DB

## Running project

Make sure next to `application.properties` there is `env.properties`.

It's ignored by git, it should have the following content:

```
api.key=<REAL API KEY TO OMDB>
```

### Docker compose

`docker-compose` is required in your system to have MySQL instance.

Just run:

```
docker-compose up -d
```

After that populate DB schema with tables and data:
```
mysql -h 127.0.0.1 -u root -p'root' < schema.sql
```

### Run tests

If everything is ready just run `mvn test` to run test suite.

### Requesting APIs

Start spring boot application by running:
```
mvn spring-boot:run
```

There are preconfigured in memory three users that can be used to make requests to APIs.

| Username  | Password   |
|-----------|------------|
| admin     | padmin     |
| customer1 | pcustomer1 |
| customer2 | pcustomer2 |

For example:
```
GET http://localhost:8080/movies/1/pricings
Authorization: Basic admin padmin
```

In repository there is `cinema.http` (IntelliJ Http Client) with all examples.
