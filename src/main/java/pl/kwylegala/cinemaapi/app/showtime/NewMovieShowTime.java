package pl.kwylegala.cinemaapi.app.showtime;

import java.time.Instant;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

@Value
public class NewMovieShowTime {

    MovieId movieId;
    CinemaRoom room;
    Instant showTime;
}
