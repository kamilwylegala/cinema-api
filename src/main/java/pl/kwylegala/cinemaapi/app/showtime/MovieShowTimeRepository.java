package pl.kwylegala.cinemaapi.app.showtime;

import java.time.Instant;
import java.util.List;

public interface MovieShowTimeRepository {

    void saveMovieShowTime(NewMovieShowTime movieShowTime);

    List<MovieShowTime> findAll();

    List<MovieShowTime> findAllByRange(Instant start, Instant end);

    void delete(int showTimeId);

    boolean showTimeExists(int showTimeId);

}
