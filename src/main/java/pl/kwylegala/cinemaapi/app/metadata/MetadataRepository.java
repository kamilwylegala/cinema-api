package pl.kwylegala.cinemaapi.app.metadata;

import java.util.Optional;

import pl.kwylegala.cinemaapi.app.movies.MovieId;

public interface MetadataRepository {

    Optional<MovieMetadata> getById(MovieId movieId);

}
