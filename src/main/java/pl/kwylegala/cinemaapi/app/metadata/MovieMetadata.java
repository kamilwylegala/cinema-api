package pl.kwylegala.cinemaapi.app.metadata;

import lombok.Value;

@Value
public class MovieMetadata {
    String title;
    int year;
    String rated;
    String released;
    String runtime;
    String genre;
    String director;
    String writer;
    String actors;
    String plot;
}
