package pl.kwylegala.cinemaapi.app.management.pricing;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

@Value
public class TicketPriceRemovalCommand {

    MovieId movieId;
}
