package pl.kwylegala.cinemaapi.app.management.showtime;

import lombok.Getter;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

@Getter
public class MovieShowTimeConflictException extends RuntimeException {
    private final MovieId existingMovieId;
    private final MovieId newMovieId;

    public MovieShowTimeConflictException(MovieId existingMovieId, MovieId newMovieId) {
        this.existingMovieId = existingMovieId;
        this.newMovieId = newMovieId;
    }

    @Override
    public String getMessage() {
        return "Movie show time for " + newMovieId.getValue() + " cannot be saved due to conflict with " +
                existingMovieId.getValue();
    }
}
