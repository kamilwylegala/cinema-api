package pl.kwylegala.cinemaapi.app.management.pricing;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing.PricingType;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

@Value
public class TicketPriceUpdateCommand {

    MovieId movieId;
    int price;
    PricingType pricingType;

}
