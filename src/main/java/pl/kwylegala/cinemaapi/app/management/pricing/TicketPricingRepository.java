package pl.kwylegala.cinemaapi.app.management.pricing;

import java.util.List;

import pl.kwylegala.cinemaapi.app.movies.MovieId;

public interface TicketPricingRepository {

    void save(TicketPricing ticketPricing);

    List<TicketPricing> findAll(MovieId movieId);

    void removePricing(MovieId movieId);
}
