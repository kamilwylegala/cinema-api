package pl.kwylegala.cinemaapi.app.management.pricing;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

@Value
public class TicketPricing {

    MovieId movieId;
    int price;
    PricingType pricingType;

    public enum PricingType {
        STANDARD,
        STUDENT,
        FAMILY
    }
}
