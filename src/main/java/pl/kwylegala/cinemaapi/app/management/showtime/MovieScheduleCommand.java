package pl.kwylegala.cinemaapi.app.management.showtime;

import java.time.Instant;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;

@Value
public class MovieScheduleCommand {

    MovieId movieId;
    CinemaRoom cinemaRoom;
    Instant showTime;

}
