package pl.kwylegala.cinemaapi.app.management.showtime;

import static pl.kwylegala.cinemaapi.app.movies.PredefinedMovieDuration.DURATIONS;

import java.time.Instant;
import java.util.List;

import pl.kwylegala.cinemaapi.app.movies.MovieNotFoundException;
import pl.kwylegala.cinemaapi.app.movies.MovieRepository;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTime;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTimeRepository;
import pl.kwylegala.cinemaapi.app.showtime.NewMovieShowTime;

import org.springframework.stereotype.Service;

@Service
public class MovieShowTimeManager {

    private final MovieShowTimeRepository movieShowTimeRepository;
    private final MovieRepository movieRepository;

    public MovieShowTimeManager(MovieShowTimeRepository movieShowTimeRepository, MovieRepository movieRepository) {
        this.movieShowTimeRepository = movieShowTimeRepository;
        this.movieRepository = movieRepository;
    }

    private static boolean isBetween(Instant start, Instant end, Instant subject) {
        return subject.compareTo(start) >= 0 && subject.compareTo(end) <= 0;
    }

    public void scheduleMovie(MovieScheduleCommand movieScheduleCommand) {
        if (!movieRepository.exists(movieScheduleCommand.getMovieId())) {
            throw new MovieNotFoundException(movieScheduleCommand.getMovieId());
        }

        NewMovieShowTime newMovieShowTime = new NewMovieShowTime(
                movieScheduleCommand.getMovieId(),
                movieScheduleCommand.getCinemaRoom(),
                movieScheduleCommand.getShowTime()
        );

        List<MovieShowTime> existingMovieTimes = movieShowTimeRepository.findAll();

        checkShowTimeConflict(
                existingMovieTimes,
                newMovieShowTime
        );

        movieShowTimeRepository.saveMovieShowTime(newMovieShowTime);
    }

    public List<MovieShowTime> getShowTimes() {
        return movieShowTimeRepository.findAll();
    }

    public void deleteShowTime(int showTimeId) {
        if (!movieShowTimeRepository.showTimeExists(showTimeId)) {
            throw new MovieShowTimeNotFoundException("Show time not found.");
        }

        movieShowTimeRepository.delete(showTimeId);
    }

    private void checkShowTimeConflict(List<MovieShowTime> existingMovieTimes, NewMovieShowTime newMovieShowTime) {
        Instant newShowTimeFinish = newMovieShowTime.getShowTime()
                .plusSeconds(
                        DURATIONS.get(newMovieShowTime.getMovieId()).getSeconds()
                );

        for (MovieShowTime movieShowTime : existingMovieTimes) {
            if (movieShowTime.getRoom() != newMovieShowTime.getRoom()) {
                continue;
            }

            Instant start = movieShowTime.getShowTime();
            Instant end = start.plusSeconds(
                    DURATIONS.get(movieShowTime.getMovieId()).getSeconds()
            );

            if (isBetween(start, end, newMovieShowTime.getShowTime()) || isBetween(start, end, newShowTimeFinish)) {
                throw new MovieShowTimeConflictException(movieShowTime.getMovieId(), newMovieShowTime.getMovieId());
            }
        }
    }
}

