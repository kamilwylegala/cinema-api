package pl.kwylegala.cinemaapi.app.management.pricing;

import java.util.List;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.movies.MovieNotFoundException;
import pl.kwylegala.cinemaapi.app.movies.MovieRepository;

import org.springframework.stereotype.Service;

@Service
public class TicketPricingManager {

    private final TicketPricingRepository ticketPricingRepository;
    private final MovieRepository movieRepository;

    public TicketPricingManager(TicketPricingRepository ticketPricingRepository, MovieRepository movieRepository) {
        this.ticketPricingRepository = ticketPricingRepository;
        this.movieRepository = movieRepository;
    }

    public void updatePrice(TicketPriceUpdateCommand ticketPriceUpdateCommand) {
        checkMovieExistence(ticketPriceUpdateCommand.getMovieId());

        ticketPricingRepository.save(
                new TicketPricing(
                        ticketPriceUpdateCommand.getMovieId(),
                        ticketPriceUpdateCommand.getPrice(),
                        ticketPriceUpdateCommand.getPricingType()
                )
        );
    }

    public List<TicketPricing> getAllPricings(MovieId movieId) {
        checkMovieExistence(movieId);

        return ticketPricingRepository.findAll(movieId);
    }

    public void removePricing(TicketPriceRemovalCommand ticketPriceRemovalCommand) {
        checkMovieExistence(ticketPriceRemovalCommand.getMovieId());

        ticketPricingRepository.removePricing(ticketPriceRemovalCommand.getMovieId());
    }

    private void checkMovieExistence(MovieId movieId) {
        if (!movieRepository.exists(movieId)) {
            throw new MovieNotFoundException(movieId);
        }
    }

}
