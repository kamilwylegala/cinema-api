package pl.kwylegala.cinemaapi.app.management.showtime;

public class MovieShowTimeNotFoundException extends RuntimeException {

    public MovieShowTimeNotFoundException(String message) {
        super(message);
    }
}
