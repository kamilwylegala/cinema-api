package pl.kwylegala.cinemaapi.app.movies;

/**
 * This could be hardcoded const like DURATIONS next to this interface but let's keep it...
 */
public interface MovieRepository {

    boolean exists(MovieId movieId);

}
