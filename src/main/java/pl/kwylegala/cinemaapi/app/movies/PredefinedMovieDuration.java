package pl.kwylegala.cinemaapi.app.movies;

import static java.time.Duration.ofMinutes;

import java.time.Duration;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class PredefinedMovieDuration {

    public static final Map<MovieId, Duration> DURATIONS = ImmutableMap.<MovieId, Duration>builder()
            .put(new MovieId(1), ofMinutes(106))
            .put(new MovieId(2), ofMinutes(107))
            .put(new MovieId(3), ofMinutes(104))
            .put(new MovieId(4), ofMinutes(107))
            .put(new MovieId(5), ofMinutes(130))
            .put(new MovieId(6), ofMinutes(130))
            .put(new MovieId(7), ofMinutes(137))
            .put(new MovieId(8), ofMinutes(136))
            .build();

}
