package pl.kwylegala.cinemaapi.app.movies;

public class MovieNotFoundException extends RuntimeException {
    private final MovieId movieId;

    public MovieNotFoundException(MovieId movieId) {
        this.movieId = movieId;
    }

    @Override
    public String getMessage() {
        return "Movie " + movieId.getValue() + " not found.";
    }
}
