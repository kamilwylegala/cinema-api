package pl.kwylegala.cinemaapi.app.movies;

import lombok.Value;

@Value
public class MovieId {

    int value;

}
