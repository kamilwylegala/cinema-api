package pl.kwylegala.cinemaapi.app.ratings;

import lombok.Value;

@Value
public class MovieRating {

    double average;

}