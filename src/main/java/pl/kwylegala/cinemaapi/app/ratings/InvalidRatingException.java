package pl.kwylegala.cinemaapi.app.ratings;

public class InvalidRatingException extends RuntimeException {

    public InvalidRatingException() {
        super("Rating must be score between 1 and 5.");
    }
}
