package pl.kwylegala.cinemaapi.app.ratings;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

@Value
public class RateMovieCommand {

    MovieId movieId;
    String username;
    Rating rating;
}
