package pl.kwylegala.cinemaapi.app.ratings;

import lombok.Value;

@Value
public class Rating {

    int points;

    public Rating(int points) {
        if (points >= 1 && points <= 5) {
            this.points = points;
        } else {
            throw new InvalidRatingException();
        }
    }
}
