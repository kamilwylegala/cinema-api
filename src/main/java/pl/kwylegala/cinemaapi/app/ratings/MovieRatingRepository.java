package pl.kwylegala.cinemaapi.app.ratings;

import pl.kwylegala.cinemaapi.app.movies.MovieId;

public interface MovieRatingRepository {

    void saveRating(MovieRatingByUser movieRatingByUser);

    MovieRating getAverageRatingForMovie(MovieId movieId);

}
