package pl.kwylegala.cinemaapi.app.ratings;

import org.springframework.stereotype.Service;

@Service
public class MovieRatingManager {

    private final MovieRatingRepository movieRatingRepository;

    public MovieRatingManager(MovieRatingRepository movieRatingRepository) {
        this.movieRatingRepository = movieRatingRepository;
    }

    public void saveMovieRatingByUser(RateMovieCommand rateMovieCommand) {
        movieRatingRepository.saveRating(
                new MovieRatingByUser(
                        rateMovieCommand.getMovieId(),
                        rateMovieCommand.getUsername(),
                        rateMovieCommand.getRating()
                )
        );
    }
}
