package pl.kwylegala.cinemaapi.app.reporting.schedule;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

import pl.kwylegala.cinemaapi.app.reporting.schedule.DaySchedule.ScheduledMovie;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTime;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTimeRepository;

import org.springframework.stereotype.Service;

@Service
public class ScheduleReporter {

    private final MovieShowTimeRepository movieShowTimeRepository;

    public ScheduleReporter(MovieShowTimeRepository movieShowTimeRepository) {
        this.movieShowTimeRepository = movieShowTimeRepository;
    }

    public DaySchedule getScheduleFor(LocalDate date) {
        Instant startOfTheDay = date.atStartOfDay()
                .toInstant(ZoneOffset.UTC);

        Instant endOfTheDay = date.atStartOfDay()
                .plus(Duration.ofDays(1))
                .minus(Duration.ofSeconds(1))
                .toInstant(ZoneOffset.UTC);

        List<MovieShowTime> movieShowTime = movieShowTimeRepository.findAllByRange(startOfTheDay, endOfTheDay);

        List<ScheduledMovie> scheduledMovies = movieShowTime.stream()
                .map(
                        mst -> new ScheduledMovie(mst.getMovieId(), mst.getRoom(), mst.getShowTime())
                )
                .collect(Collectors.toList());

        return new DaySchedule(scheduledMovies);

    }

}
