package pl.kwylegala.cinemaapi.app.reporting.schedule;

import java.time.Instant;
import java.util.List;

import lombok.Value;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;

@Value
public class DaySchedule {

    List<ScheduledMovie> scheduledMovies;

    @Value
    public static class ScheduledMovie {
        MovieId movieId;
        CinemaRoom room;
        Instant showTime;
    }
}
