package pl.kwylegala.cinemaapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleApi {

    @GetMapping("/hello")
    public String get() {
        return "hello";
    }

}
