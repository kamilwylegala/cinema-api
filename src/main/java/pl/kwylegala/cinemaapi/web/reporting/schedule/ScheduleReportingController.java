package pl.kwylegala.cinemaapi.web.reporting.schedule;

import static pl.kwylegala.cinemaapi.framework.spring.SecurityConfig.ROLE_CUSTOMER;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.kwylegala.cinemaapi.app.reporting.schedule.DaySchedule;
import pl.kwylegala.cinemaapi.app.reporting.schedule.ScheduleReporter;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Secured({ ROLE_CUSTOMER })
@RequiredArgsConstructor
@Validated
public class ScheduleReportingController {

    private final ScheduleReporter scheduleReporter;

    @GetMapping("/schedules")
    public DayScheduleDto getDaySchedule(@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("date") @NotNull LocalDate localDate) {
        DaySchedule daySchedule = scheduleReporter.getScheduleFor(localDate);

        return new DayScheduleDto(
                daySchedule.getScheduledMovies()
                        .stream()
                        .map(scheduledMovie -> new ScheduledMovieDto(
                                scheduledMovie.getMovieId().getValue(),
                                scheduledMovie.getRoom(),
                                scheduledMovie.getShowTime()
                        ))
                        .collect(Collectors.toList())
        );
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class DayScheduleDto {
        List<ScheduledMovieDto> movies;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class ScheduledMovieDto {
        int movieId;
        CinemaRoom room;
        Instant showTime;
    }
}
