package pl.kwylegala.cinemaapi.web.metadata;

import static pl.kwylegala.cinemaapi.framework.spring.SecurityConfig.ROLE_CUSTOMER;

import pl.kwylegala.cinemaapi.app.metadata.MetadataRepository;
import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.web.exceptions.ResourceNotFoundException;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Secured({ ROLE_CUSTOMER })
public class MoviesMetadataController {

    private final MetadataRepository metadataRepository;

    public MoviesMetadataController(MetadataRepository metadataRepository) {
        this.metadataRepository = metadataRepository;
    }

    @GetMapping(path = "/movies/{movieId}/metadata")
    public MovieMetadata get(@PathVariable("movieId") int movieId) {
        return metadataRepository.getById(new MovieId(movieId))
                .orElseThrow(() -> new ResourceNotFoundException("Movie metadata for id " + movieId + " was not found."));
    }

}
