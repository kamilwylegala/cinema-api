package pl.kwylegala.cinemaapi.web.ratings;

import static pl.kwylegala.cinemaapi.framework.spring.SecurityConfig.ROLE_CUSTOMER;

import lombok.Data;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.ratings.MovieRatingManager;
import pl.kwylegala.cinemaapi.app.ratings.RateMovieCommand;
import pl.kwylegala.cinemaapi.app.ratings.Rating;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Secured({ ROLE_CUSTOMER })
public class MovieRatingController {

    private final MovieRatingManager movieRatingManager;

    public MovieRatingController(MovieRatingManager movieRatingManager) {
        this.movieRatingManager = movieRatingManager;
    }

    @PutMapping("/movies/{movieId}/ratings")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void saveMovieRating(
            @PathVariable("movieId") int movieId,
            @RequestBody RateMovieDto rateMovieDto,
            Authentication authentication
    ) {
        movieRatingManager.saveMovieRatingByUser(
                new RateMovieCommand(
                        new MovieId(movieId),
                        authentication.getName(),
                        new Rating(rateMovieDto.getRating())
                )
        );
    }

    @Data
    private static class RateMovieDto {
        int rating;
    }

}
