package pl.kwylegala.cinemaapi.web.exceptions;

import pl.kwylegala.cinemaapi.app.management.showtime.MovieShowTimeConflictException;
import pl.kwylegala.cinemaapi.app.management.showtime.MovieShowTimeNotFoundException;
import pl.kwylegala.cinemaapi.app.movies.MovieNotFoundException;
import pl.kwylegala.cinemaapi.app.ratings.InvalidRatingException;
import pl.kwylegala.cinemaapi.framework.repository.ExternalServiceNotAvailableException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlers {

    private static ResponseEntity<Object> getResponseEntity(HttpStatus conflict, String message) {
        return ResponseEntity
                .status(conflict)
                .contentType(MediaType.APPLICATION_JSON)
                .body(String.format("{\"error\":\"%s\"}", message));
    }

    @ExceptionHandler({ ExternalServiceNotAvailableException.class })
    public ResponseEntity<Object> handleExternalServiceNotAvailableException(ExternalServiceNotAvailableException ex) {
        return getResponseEntity(HttpStatus.SERVICE_UNAVAILABLE, ex.getMessage());
    }

    @ExceptionHandler({ InvalidRatingException.class })
    public ResponseEntity<Object> handleBadRequest(InvalidRatingException ex) {
        return getResponseEntity(HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler({ MovieNotFoundException.class, MovieShowTimeNotFoundException.class })
    public ResponseEntity<Object> handleNotFoundException(RuntimeException ex) {
        return getResponseEntity(HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler({ MovieShowTimeConflictException.class })
    public ResponseEntity<Object> handleMovieShowTimeConflictException(MovieShowTimeConflictException ex) {
        return getResponseEntity(HttpStatus.CONFLICT, ex.getMessage());
    }

}
