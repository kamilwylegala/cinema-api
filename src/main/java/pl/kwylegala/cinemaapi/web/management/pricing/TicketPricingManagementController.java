package pl.kwylegala.cinemaapi.web.management.pricing;

import static pl.kwylegala.cinemaapi.framework.spring.SecurityConfig.ROLE_ADMIN;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.Value;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPriceRemovalCommand;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPriceUpdateCommand;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing.PricingType;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricingManager;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Secured({ ROLE_ADMIN })
public class TicketPricingManagementController {

    private final TicketPricingManager ticketPricingManager;

    public TicketPricingManagementController(TicketPricingManager ticketPricingManager) {
        this.ticketPricingManager = ticketPricingManager;
    }

    @PostMapping(path = "/movies/{movieId}/pricings")
    public void updateTicketPricing(@PathVariable("movieId") int movieId, @RequestBody TicketPriceUpdateDto dto) {
        TicketPriceUpdateCommand command = new TicketPriceUpdateCommand(new MovieId(movieId), dto.getPrice(), dto.getType());

        ticketPricingManager.updatePrice(command);
    }

    @GetMapping(path = "/movies/{movieId}/pricings")
    public List<TicketPricingDto> getPricingsForMovie(@PathVariable("movieId") int movieId) {
        return ticketPricingManager.getAllPricings(new MovieId(movieId))
                .stream()
                .map(ticketPricing -> new TicketPricingDto(ticketPricing.getPricingType(), ticketPricing.getPrice()))
                .collect(Collectors.toList());
    }

    @DeleteMapping(path = "/movies/{movieId}/pricings")
    public void removeAllTicketPricings(@PathVariable("movieId") int movieId) {
        TicketPriceRemovalCommand command = new TicketPriceRemovalCommand(new MovieId(movieId));

        ticketPricingManager.removePricing(command);
    }

    @Data
    private static class TicketPriceUpdateDto {
        PricingType type;
        int price;
    }

    @Value
    private static class TicketPricingDto {
        PricingType type;
        int price;
    }

}
