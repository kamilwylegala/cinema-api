package pl.kwylegala.cinemaapi.web.management.showtime;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static pl.kwylegala.cinemaapi.framework.spring.SecurityConfig.ROLE_ADMIN;

import java.time.Instant;
import java.util.List;

import lombok.Data;
import lombok.Value;
import pl.kwylegala.cinemaapi.app.management.showtime.MovieScheduleCommand;
import pl.kwylegala.cinemaapi.app.management.showtime.MovieShowTimeManager;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Secured({ ROLE_ADMIN })
public class ShowTimeManagementController {

    private final MovieShowTimeManager movieShowTimeManager;

    public ShowTimeManagementController(MovieShowTimeManager movieShowTimeManager) {
        this.movieShowTimeManager = movieShowTimeManager;
    }

    @PostMapping(path = "/shows")
    @ResponseStatus(NO_CONTENT)
    public void scheduleMovie(@RequestBody ScheduleMovieDto scheduleMovieDto) {
        movieShowTimeManager.scheduleMovie(
                new MovieScheduleCommand(
                        new MovieId(scheduleMovieDto.getMovieId()),
                        scheduleMovieDto.getRoom(),
                        scheduleMovieDto.getShowTime()
                )
        );
    }

    @GetMapping(path = "/shows")
    public List<MovieShowTimeDto> getShows() {
        return movieShowTimeManager.getShowTimes()
                .stream()
                .map(showTime -> new MovieShowTimeDto(
                        showTime.getId(),
                        showTime.getMovieId().getValue(),
                        showTime.getRoom(),
                        showTime.getShowTime()
                ))
                .collect(toList());
    }

    @DeleteMapping(path = "/shows/{showTimeId}")
    @ResponseStatus(NO_CONTENT)
    public void deleteShowTime(@PathVariable("showTimeId") int showTimeId) {
        movieShowTimeManager.deleteShowTime(showTimeId);
    }

    @Data
    private static class ScheduleMovieDto {
        int movieId;
        CinemaRoom room;
        Instant showTime;
    }

    @Value
    static class MovieShowTimeDto {
        int id;
        int movieId;
        CinemaRoom room;
        Instant showTime;
    }

}
