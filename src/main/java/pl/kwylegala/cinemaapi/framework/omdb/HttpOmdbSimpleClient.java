package pl.kwylegala.cinemaapi.framework.omdb;

import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

@Component
public class HttpOmdbSimpleClient implements OmdbSimpleClient {

    @Override
    public SimplifiedResponse get(String url) {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(url);

            try (CloseableHttpResponse response = client.execute(httpGet)) {
                return new SimplifiedResponse(
                        response.getStatusLine().getStatusCode(),
                        EntityUtils.toString(response.getEntity())
                );
            }

        } catch (IOException c) {
            throw new RuntimeException(c);
        }
    }
}
