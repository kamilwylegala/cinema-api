package pl.kwylegala.cinemaapi.framework.omdb;

import lombok.Value;

interface OmdbSimpleClient {

    SimplifiedResponse get(String url);

    @Value
    class SimplifiedResponse {
        int statusCode;
        String response;
    }

}
