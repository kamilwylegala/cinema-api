package pl.kwylegala.cinemaapi.framework.omdb;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;

import lombok.Data;
import lombok.ToString;
import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;
import pl.kwylegala.cinemaapi.framework.omdb.OmdbSimpleClient.SimplifiedResponse;
import pl.kwylegala.cinemaapi.framework.repository.ExternalMovieMetadataProvider;
import pl.kwylegala.cinemaapi.framework.repository.ExternalServiceNotAvailableException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.evanlennick.retry4j.CallExecutor;
import com.evanlennick.retry4j.CallExecutorBuilder;
import com.evanlennick.retry4j.Status;
import com.evanlennick.retry4j.config.RetryConfig;
import com.evanlennick.retry4j.config.RetryConfigBuilder;
import com.evanlennick.retry4j.exception.RetriesExhaustedException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class OmdbExternalMovieMetadataProvider implements ExternalMovieMetadataProvider {

    private final Logger logger = LoggerFactory.getLogger(OmdbExternalMovieMetadataProvider.class);

    private final OmdbSimpleClient omdbSimpleClient;
    private final Duration delayBetweenAttempts;
    private final String omdbApiKey;
    private final ObjectMapper objectMapper = new ObjectMapper();

    OmdbExternalMovieMetadataProvider(
            OmdbSimpleClient omdbSimpleClient,
            Duration delayBetweenAttempts,
            String omdbApiKey
    ) {
        this.omdbSimpleClient = omdbSimpleClient;
        this.delayBetweenAttempts = delayBetweenAttempts;
        this.omdbApiKey = omdbApiKey;
    }

    @Override
    public Optional<MovieMetadata> getById(String externalMoveId) {
        RetryConfig retryConfig = new RetryConfigBuilder()
                //TODO: Retry only on connection exceptions
                //TODO: Retry on 5xx errors
                .retryOnAnyException()
                .withMaxNumberOfTries(5)
                .withFibonacciBackoff()
                .withDelayBetweenTries(delayBetweenAttempts)
                .build();

        CallExecutor<SimplifiedResponse> callExecutor = new CallExecutorBuilder<>()
                .config(retryConfig)
                .build();

        String rawJson;
        try {
            Status<SimplifiedResponse> status = callExecutor.execute(() -> makeHttpCallToGetMetadata(externalMoveId));

            rawJson = status.getResult().getResponse();
        } catch (RetriesExhaustedException ree) {
            logger.error("Retry requests to omdb exhausted", ree);
            throw new ExternalServiceNotAvailableException("Service is temporarily unavailable. Please try again later.");
        }

        try {
            MovieMetadataDto dto = convertEntityToObject(rawJson);

            return Optional.of(dto.toMovieMetadata());
        } catch (IOException ex) {
            logger.error("Error during generating metadata", ex);
            return Optional.empty();
        }
    }

    private SimplifiedResponse makeHttpCallToGetMetadata(String externalMoveId) {
        //TODO: Put api key to env.
        return omdbSimpleClient.get("http://www.omdbapi.com/?apikey=" + omdbApiKey + "&i=" + externalMoveId);
    }

    private MovieMetadataDto convertEntityToObject(String json) throws IOException {
        return objectMapper.readValue(json, MovieMetadataDto.class);
    }

    @ToString
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class MovieMetadataDto {
        @JsonProperty("Title")
        String title;
        @JsonProperty("Year")
        int year;
        @JsonProperty("Rated")
        String rated;
        @JsonProperty("Released")
        String released;
        @JsonProperty("Runtime")
        String runtime;
        @JsonProperty("Genre")
        String genre;
        @JsonProperty("Director")
        String director;
        @JsonProperty("Writer")
        String writer;
        @JsonProperty("Actors")
        String actors;
        @JsonProperty("Plot")
        String plot;

        MovieMetadata toMovieMetadata() {
            return new MovieMetadata(
                    title, year, rated, released, runtime, genre, director, writer, actors, plot
            );
        }
    }

}
