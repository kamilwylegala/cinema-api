package pl.kwylegala.cinemaapi.framework.repository;

import java.util.List;

import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing.PricingType;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricingRepository;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Service;

@Service
public class JdbcTicketPricingRepository implements TicketPricingRepository {

    private final JdbcTemplate jdbcTemplate;

    public JdbcTicketPricingRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(TicketPricing ticketPricing) {
        jdbcTemplate.execute(
                "INSERT INTO pricing (movie_id, type, price) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `type` = ?, `price` = ?",
                (PreparedStatementCallback<Boolean>) ps -> {
                    ps.setInt(1, ticketPricing.getMovieId().getValue());
                    ps.setString(2, ticketPricing.getPricingType().name());
                    ps.setInt(3, ticketPricing.getPrice());
                    ps.setString(4, ticketPricing.getPricingType().name());
                    ps.setInt(5, ticketPricing.getPrice());
                    return ps.execute();
                }
        );
    }

    @Override
    public List<TicketPricing> findAll(MovieId movieId) {
        return jdbcTemplate.query(
                "SELECT * FROM pricing WHERE movie_id = ?",
                ps -> ps.setInt(1, movieId.getValue()),
                (row, num) -> {
                    return new TicketPricing(
                            new MovieId(row.getInt("movie_id")),
                            row.getInt("price"),
                            PricingType.valueOf(row.getString("type"))
                    );
                }
        );
    }

    @Override
    public void removePricing(MovieId movieId) {
        jdbcTemplate.execute(
                "DELETE FROM pricing WHERE movie_id = ?",
                (PreparedStatementCallback<Boolean>) ps -> {
                    ps.setInt(1, movieId.getValue());
                    return ps.execute();
                }
        );
    }
}
