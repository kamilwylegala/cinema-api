package pl.kwylegala.cinemaapi.framework.repository;

public class ExternalServiceNotAvailableException extends RuntimeException {

    public ExternalServiceNotAvailableException(String message) {
        super(message);
    }
}
