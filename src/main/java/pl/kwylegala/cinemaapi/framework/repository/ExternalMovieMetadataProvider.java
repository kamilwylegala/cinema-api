package pl.kwylegala.cinemaapi.framework.repository;

import java.util.Optional;

import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;

public interface ExternalMovieMetadataProvider {
    Optional<MovieMetadata> getById(String externalMoveId);
}
