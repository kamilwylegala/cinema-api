package pl.kwylegala.cinemaapi.framework.repository;

import java.util.List;
import java.util.Optional;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.movies.MovieRepository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class MovieIdMapper implements MovieRepository {

    private final JdbcTemplate jdbcTemplate;

    public MovieIdMapper(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<String> getExternalId(MovieId movieId) {
        List<String> externalIds = jdbcTemplate.query(
                "select external_id from external_movies_mapping where movie_id = ?",
                ps -> ps.setInt(1, movieId.getValue()),
                (row, num) -> row.getString(1)
        );

        if (externalIds.size() == 0) {
            return Optional.empty();
        }

        return Optional.of(externalIds.get(0));
    }

    @Override
    public boolean exists(MovieId movieId) {
        return getExternalId(movieId).isPresent();
    }
}
