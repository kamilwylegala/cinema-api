package pl.kwylegala.cinemaapi.framework.repository;

import java.util.Optional;

import pl.kwylegala.cinemaapi.app.metadata.MetadataRepository;
import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

import org.springframework.stereotype.Component;

@Component
public class ExternalMappingBasedMetadataRepository implements MetadataRepository {

    private final ExternalMovieMetadataProvider externalMovieMetadataProvider;
    private final MovieIdMapper moveIdMapper;

    public ExternalMappingBasedMetadataRepository(
            ExternalMovieMetadataProvider externalMovieMetadataProvider,
            MovieIdMapper moveIdMapper
    ) {
        this.externalMovieMetadataProvider = externalMovieMetadataProvider;
        this.moveIdMapper = moveIdMapper;
    }

    @Override
    public Optional<MovieMetadata> getById(MovieId movieId) {
        Optional<String> externalMoveId = moveIdMapper.getExternalId(movieId);

        if (!externalMoveId.isPresent()) {
            return Optional.empty();
        }

        return externalMovieMetadataProvider.getById(externalMoveId.get());
    }
}
