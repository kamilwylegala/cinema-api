package pl.kwylegala.cinemaapi.framework.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.ratings.MovieRating;
import pl.kwylegala.cinemaapi.app.ratings.MovieRatingByUser;
import pl.kwylegala.cinemaapi.app.ratings.MovieRatingRepository;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Service;

@Service
public class JdbcMovieRatingRepository implements MovieRatingRepository {

    private final JdbcTemplate jdbcTemplate;

    public JdbcMovieRatingRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void saveRating(MovieRatingByUser movieRatingByUser) {
        jdbcTemplate.execute(
                "INSERT INTO ratings (movie_id, username, rating) VALUES (?,?,?) ON DUPLICATE KEY UPDATE rating = ?",
                new PreparedStatementCallback<Boolean>() {
                    @Override
                    public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                        ps.setInt(1, movieRatingByUser.getMovieId().getValue());
                        ps.setString(2, movieRatingByUser.getUsername());
                        ps.setInt(3, movieRatingByUser.getRating().getPoints());
                        ps.setInt(4, movieRatingByUser.getRating().getPoints());
                        return ps.execute();
                    }
                }
        );
    }

    @Override
    public MovieRating getAverageRatingForMovie(MovieId movieId) {
        double avg = jdbcTemplate.queryForObject(
                "SELECT AVG(rating) FROM ratings WHERE movie_id = ?",
                Double.class,
                movieId.getValue()
        );

        return new MovieRating(avg);
    }
}
