package pl.kwylegala.cinemaapi.framework.repository;

import java.time.Instant;
import java.util.List;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTime;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTimeRepository;
import pl.kwylegala.cinemaapi.app.showtime.NewMovieShowTime;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class JdbcMovieShowTimeRepository implements MovieShowTimeRepository {

    private static final RowMapper<MovieShowTime> MOVIE_SHOW_TIME_ROW_MAPPER = (row, num) -> new MovieShowTime(
            row.getInt("id"),
            new MovieId(row.getInt("movie_id")),
            CinemaRoom.valueOf(row.getString("room")),
            Instant.ofEpochSecond(row.getLong("show_time"))
    );
    private final JdbcTemplate jdbcTemplate;

    public JdbcMovieShowTimeRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void saveMovieShowTime(NewMovieShowTime movieShowTime) {
        jdbcTemplate.execute(
                "INSERT INTO show_times (movie_id, room, show_time) VALUES (?, ?, ?)",
                (PreparedStatementCallback<Boolean>) ps -> {
                    ps.setInt(1, movieShowTime.getMovieId().getValue());
                    ps.setString(2, movieShowTime.getRoom().name());
                    ps.setLong(3, movieShowTime.getShowTime().getEpochSecond());

                    return ps.execute();
                }
        );
    }

    @Override
    public List<MovieShowTime> findAll() {
        return jdbcTemplate.query(
                "SELECT * FROM show_times ORDER BY show_time ASC",
                MOVIE_SHOW_TIME_ROW_MAPPER
        );
    }

    @Override
    public List<MovieShowTime> findAllByRange(Instant start, Instant end) {
        return jdbcTemplate.query(
                "SELECT * FROM show_times WHERE show_time >= ? AND show_time <= ? ORDER BY show_time ASC",
                ps -> {
                    ps.setLong(1, start.getEpochSecond());
                    ps.setLong(2, end.getEpochSecond());
                },
                MOVIE_SHOW_TIME_ROW_MAPPER
        );
    }

    @Override
    public void delete(int showTimeId) {
        jdbcTemplate.execute(
                "DELETE FROM show_times WHERE id = ?",
                (PreparedStatementCallback<Boolean>) ps -> {
                    ps.setInt(1, showTimeId);
                    return ps.execute();
                }
        );

    }

    public boolean showTimeExists(int showTimeId) {
        Integer count = jdbcTemplate.queryForObject(
                "SELECT count(*) FROM show_times WHERE id = ?",
                Integer.class,
                showTimeId
        );

        return count > 0;
    }
}
