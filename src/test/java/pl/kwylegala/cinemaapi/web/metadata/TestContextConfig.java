package pl.kwylegala.cinemaapi.web.metadata;

import pl.kwylegala.cinemaapi.framework.repository.ExternalMovieMetadataProvider;
import pl.kwylegala.cinemaapi.framework.spring.FrameworkContextConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

@Configuration
@Import(FrameworkContextConfig.class)
public class TestContextConfig {

    @Bean
    @Primary
    ExternalMovieMetadataProvider externalMovieMetadataProvider() {
        return new FakeExternalMovieMetadataProvider();
    }

}
