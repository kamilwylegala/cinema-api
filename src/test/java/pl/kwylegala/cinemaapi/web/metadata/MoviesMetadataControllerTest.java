package pl.kwylegala.cinemaapi.web.metadata;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.kwylegala.cinemaapi.web.metadata.FakeExternalMovieMetadataProvider.MOCKED_METADATA;

import pl.kwylegala.cinemaapi.app.metadata.MetadataRepository;
import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;
import pl.kwylegala.cinemaapi.framework.repository.ExternalServiceNotAvailableException;
import pl.kwylegala.cinemaapi.web.exceptions.ExceptionHandlers;
import pl.kwylegala.cinemaapi.web.exceptions.ResourceNotFoundException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {
        MoviesMetadataController.class,
        TestContextConfig.class }
)
@WebMvcTest
class MoviesMetadataControllerTest {

    @Autowired
    private FakeExternalMovieMetadataProvider externalMovieMetadataProvider;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MetadataRepository metadataRepository;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new MoviesMetadataController(metadataRepository))
                .setControllerAdvice(ExceptionHandlers.class)
                .build();
    }

    @AfterEach
    void tearDown() {
        externalMovieMetadataProvider.reset();
    }

    @Test
    void should_fetch_movie_metadata() throws Exception {
        externalMovieMetadataProvider.setExpectedResponse(() -> MOCKED_METADATA);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/movies/1/metadata"))
                .andExpect(status().isOk())
                .andReturn();

        MovieMetadata actual = objectMapper.readValue(result.getResponse().getContentAsString(), MovieMetadata.class);

        assertThat(actual).isEqualTo(MOCKED_METADATA);
    }

    @Test
    public void should_return_404_if_movie_metadata_was_not_found() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/movies/1999/metadata"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResolvedException()).isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    public void should_return_503_if_external_service_is_not_available() throws Exception {
        externalMovieMetadataProvider.setExpectedResponse(() -> {
            throw new ExternalServiceNotAvailableException("Unavailable");
        });

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/movies/1/metadata"))
                .andExpect(status().isServiceUnavailable())
                .andReturn();

        assertThat(result.getResolvedException()).isInstanceOf(ExternalServiceNotAvailableException.class);
    }

}