package pl.kwylegala.cinemaapi.web.metadata;

import java.util.Optional;
import java.util.function.Supplier;

import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;
import pl.kwylegala.cinemaapi.framework.repository.ExternalMovieMetadataProvider;

class FakeExternalMovieMetadataProvider implements ExternalMovieMetadataProvider {

    static final String AVAILABLE_EXTERNAL_MOVIE_ID = "tt0232500";

    static final MovieMetadata MOCKED_METADATA = new MovieMetadata(
            "Mocked movie title",
            2021,
            "PG13",
            "Last year",
            "1h",
            "horror",
            "Unknown",
            "Unknown",
            "None",
            "No idea"
    );

    private Supplier<MovieMetadata> expectedResponse;

    public void setExpectedResponse(Supplier<MovieMetadata> expectedResponse) {
        this.expectedResponse = expectedResponse;
    }

    public void reset() {
        expectedResponse = null;
    }

    @Override
    public Optional<MovieMetadata> getById(String externalMoveId) {
        if (expectedResponse == null) {
            throw new RuntimeException("Expected response not set on FakeExternalMovieMetadataProvider");
        }

        return Optional.ofNullable(expectedResponse.get());
    }
}
