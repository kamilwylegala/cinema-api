package pl.kwylegala.cinemaapi.web.ratings;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import lombok.SneakyThrows;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.ratings.MovieRating;
import pl.kwylegala.cinemaapi.app.ratings.MovieRatingRepository;
import pl.kwylegala.cinemaapi.web.metadata.MoviesMetadataController;
import pl.kwylegala.cinemaapi.web.metadata.TestContextConfig;

import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {
        MoviesMetadataController.class,
        TestContextConfig.class }
)
@WebMvcTest
class MovieRatingControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private MovieRatingRepository movieRatingRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @SneakyThrows
    private static String getRatingRequestJson(int rating) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rating", rating);
        return jsonObject.toString();
    }

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.execute("DELETE FROM ratings;");
    }

    @Test
    void should_save_rating_by_user() throws Exception {
        mockMvc.perform(put("/movies/1/ratings")
                                .contentType(APPLICATION_JSON)
                                .content(getRatingRequestJson(5))
                                .with(httpBasic("customer2", "pcustomer2"))
        ).andExpect(status().isNoContent());

        MovieRating movieRating = movieRatingRepository.getAverageRatingForMovie(new MovieId(1));

        assertThat(movieRating.getAverage()).isEqualTo(5d);
    }

    @Test
    void should_get_400_if_rating_is_invalid() throws Exception {
        mockMvc.perform(put("/movies/1/ratings")
                                .contentType(APPLICATION_JSON)
                                .content(getRatingRequestJson(10))
                                .with(httpBasic("customer2", "pcustomer2"))
        ).andExpect(status().isBadRequest());
    }

}