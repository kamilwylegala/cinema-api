package pl.kwylegala.cinemaapi.web.management.showtime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static pl.kwylegala.cinemaapi.app.showtime.CinemaRoom.CINEMA_ROOM_1;

import java.util.List;

import pl.kwylegala.cinemaapi.app.management.showtime.MovieShowTimeManager;
import pl.kwylegala.cinemaapi.framework.spring.FrameworkContextConfig;
import pl.kwylegala.cinemaapi.web.exceptions.ExceptionHandlers;
import pl.kwylegala.cinemaapi.web.management.showtime.ShowTimeManagementController.MovieShowTimeDto;

import org.assertj.core.groups.Tuple;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@ContextConfiguration(classes = { FrameworkContextConfig.class })
@WebMvcTest
class ShowTimeManagementControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MovieShowTimeManager movieShowTimeManager;

    private static String getNewShowTimeJson() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("movieId", 1);
        jsonObject.put("room", CINEMA_ROOM_1.name());
        jsonObject.put("showTime", 1636712593);
        return jsonObject.toString();
    }

    @BeforeEach
    void setUp() {
        mockMvc =
                standaloneSetup(new ShowTimeManagementController(movieShowTimeManager))
                        .setControllerAdvice(ExceptionHandlers.class)
                        .build();
    }

    @Test
    void should_create_show_time_for_movie_in_specific_cinema_room() throws Exception {
        String newShowTimeJson = getNewShowTimeJson();

        MvcResult result = createShowTime(newShowTimeJson);

        assertThat(result.getResponse().getStatus())
                .isEqualTo(NO_CONTENT.value());

        List<MovieShowTimeDto> showTimeListing = getShowTimeListing();
        assertThat(showTimeListing)
                .extracting(MovieShowTimeDto::getMovieId, MovieShowTimeDto::getRoom)
                .contains(new Tuple(1, CINEMA_ROOM_1));

        MvcResult deleteResponse = deleteShowTime(showTimeListing.get(0).getId());
        assertThat(deleteResponse.getResponse().getStatus())
                .isEqualTo(NO_CONTENT.value());
    }

    @Test
    void should_response_with_404_if_show_time_to_delete_does_not_exist() throws Exception {
        MvcResult deleteResponse = deleteShowTime(9999);
        assertThat(deleteResponse.getResponse().getStatus())
                .isEqualTo(NOT_FOUND.value());
    }

    @Test
    void should_response_with_409_if_show_time_to_save_overlaps_with_existing_scheduled_movie() throws Exception {
        String newShowTimeJson = getNewShowTimeJson();
        createShowTime(newShowTimeJson);
        MvcResult result = createShowTime(newShowTimeJson);
        assertThat(result.getResponse().getStatus()).isEqualTo(CONFLICT.value());

        List<MovieShowTimeDto> showTimeListing = getShowTimeListing();
        deleteShowTime(showTimeListing.get(0).getId());
    }

    private MvcResult createShowTime(String newShowTimeJson) throws Exception {
        MvcResult result = mockMvc.perform(
                post("/shows")
                        .contentType(APPLICATION_JSON)
                        .content(newShowTimeJson)
        ).andReturn();
        return result;
    }

    private List<MovieShowTimeDto> getShowTimeListing() throws Exception {
        MvcResult listingResponse = mockMvc.perform(get("/shows").accept(APPLICATION_JSON))
                .andReturn();

        List<MovieShowTimeDto> responseObjects = objectMapper.readValue(
                listingResponse.getResponse().getContentAsString(),
                new TypeReference<List<MovieShowTimeDto>>() {
                }
        );

        return responseObjects;
    }

    private MvcResult deleteShowTime(int showTimeId) throws Exception {
        return mockMvc.perform(delete(
                        "/shows/{showTimeId}",
                        showTimeId
                ))
                .andReturn();

    }

}