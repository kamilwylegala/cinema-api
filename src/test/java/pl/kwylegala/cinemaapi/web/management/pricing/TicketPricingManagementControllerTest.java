package pl.kwylegala.cinemaapi.web.management.pricing;

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import lombok.SneakyThrows;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPriceRemovalCommand;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing.PricingType;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricingManager;
import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.framework.spring.FrameworkContextConfig;
import pl.kwylegala.cinemaapi.web.exceptions.ExceptionHandlers;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@AutoConfigureMockMvc
@ContextConfiguration(classes = { FrameworkContextConfig.class })
@WebMvcTest
class TicketPricingManagementControllerTest {

    @Autowired
    private TicketPricingManager ticketPricingManager;
    private MockMvc mockMvc;

    @SneakyThrows(JSONException.class)
    private static String getUpdateJson(int movieId, int price, PricingType pricingType) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("movieId", movieId);
        jsonObject.put("price", price);
        jsonObject.put("type", pricingType.toString());
        return jsonObject.toString();
    }

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(new TicketPricingManagementController(ticketPricingManager))
                .setControllerAdvice(ExceptionHandlers.class)
                .build();
    }

    @AfterEach
    void tearDown() {
        ticketPricingManager.removePricing(new TicketPriceRemovalCommand(new MovieId(2)));
    }

    @Test
    public void should_successfully_add_pricing_for_movie() throws Exception {
        sendUpdateRequest("2", 3, PricingType.STANDARD);

        MvcResult result = getPricingListing();

        assertEquals("[{\"type\":\"STANDARD\",\"price\":3}]", result.getResponse().getContentAsString(), true);
    }

    @Test
    public void should_return_404_if_movie_does_not_exist() throws Exception {
        mockMvc.perform(post("/movies/1111/pricings")
                                .content(getUpdateJson(2, 3, PricingType.STANDARD))
                                .contentType("application/json")
        ).andExpect(status().isNotFound());
    }

    @Test
    public void should_update_pricing_with_two_positions_for_movie() throws Exception {
        sendUpdateRequest("2", 3, PricingType.STANDARD);
        sendUpdateRequest("2", 2, PricingType.STUDENT);

        MvcResult result = getPricingListing();

        assertEquals(
                "[{\"type\":\"STANDARD\",\"price\":3}, {\"type\":\"STUDENT\",\"price\":2}]",
                result.getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void should_remove_all_pricings_for_movie() throws Exception {
        sendUpdateRequest("2", 3, PricingType.STANDARD);
        sendUpdateRequest("2", 2, PricingType.STUDENT);

        mockMvc.perform(MockMvcRequestBuilders.delete("/movies/2/pricings"));

        MvcResult result = getPricingListing();

        assertEquals(
                "[]",
                result.getResponse().getContentAsString(),
                true
        );
    }

    private MvcResult getPricingListing() throws Exception {
        return mockMvc.perform(
                        get("/movies/2/pricings")
                                .accept("application/json")
                )
                .andReturn();
    }

    private void sendUpdateRequest(String movieId, int i, PricingType standard) throws Exception {
        mockMvc.perform(post("/movies/" + movieId + "/pricings")
                                .content(getUpdateJson(2, i, standard))
                                .contentType("application/json")
        );
    }

}