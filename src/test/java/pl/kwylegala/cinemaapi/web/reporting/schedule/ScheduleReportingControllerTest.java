package pl.kwylegala.cinemaapi.web.reporting.schedule;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.reporting.schedule.ScheduleReporter;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTimeRepository;
import pl.kwylegala.cinemaapi.app.showtime.NewMovieShowTime;
import pl.kwylegala.cinemaapi.framework.spring.FrameworkContextConfig;
import pl.kwylegala.cinemaapi.web.reporting.schedule.ScheduleReportingController.DayScheduleDto;
import pl.kwylegala.cinemaapi.web.reporting.schedule.ScheduleReportingController.ScheduledMovieDto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {
        ScheduleReportingController.class,
        FrameworkContextConfig.class }
)
@WebMvcTest
class ScheduleReportingControllerTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ScheduleReporter scheduleReporter;
    @Autowired
    private MovieShowTimeRepository movieShowTimeRepository;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(new ScheduleReportingController(scheduleReporter))
                .build();
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.execute("DELETE FROM show_times;");
    }

    @Test
    void should_present_list_of_scheduled_movies_for_given_day() throws Exception {
        // given
        Instant showTime = Instant.from(LocalDateTime.parse("2021-11-13T20:00:00").atZone(ZoneId.of("UTC")));

        movieShowTimeRepository.saveMovieShowTime(
                new NewMovieShowTime(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, showTime)
        );

        // when
        MvcResult result = mockMvc.perform(get("/schedules?date=2021-11-13"))
                .andReturn();

        // then
        DayScheduleDto dayScheduleDto = objectMapper.readValue(result.getResponse().getContentAsString(), DayScheduleDto.class);

        assertThat(dayScheduleDto)
                .isEqualTo(
                        new DayScheduleDto(
                                singletonList(
                                        new ScheduledMovieDto(
                                                1,
                                                CinemaRoom.CINEMA_ROOM_1,
                                                showTime
                                        ))
                        )
                );
    }

    @Test
    void should_return_400_if_query_param_is_not_provided() throws Exception {
        // when + then
        mockMvc.perform(get("/schedules"))
                .andExpect(status().isBadRequest());
    }
}