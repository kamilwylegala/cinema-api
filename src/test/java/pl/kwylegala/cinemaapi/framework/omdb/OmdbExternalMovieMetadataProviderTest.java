package pl.kwylegala.cinemaapi.framework.omdb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.Duration;
import java.util.Optional;

import pl.kwylegala.cinemaapi.app.metadata.MovieMetadata;
import pl.kwylegala.cinemaapi.framework.repository.ExternalServiceNotAvailableException;
import pl.kwylegala.cinemaapi.framework.spring.FrameworkContextConfig;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = FrameworkContextConfig.class)
class OmdbExternalMovieMetadataProviderTest {

    private static final String EXTERNAL_MOVE_ID = "tt0232500";
    private static final String NOT_REQUIRED = "";

    @Value("${api.key}")
    String realApiKey;

    @Test
    public void should_fetch_real_metadata_from_omdb_for_movie() {
        OmdbExternalMovieMetadataProvider sut = new OmdbExternalMovieMetadataProvider(
                new HttpOmdbSimpleClient(),
                Duration.ofSeconds(1),
                realApiKey
        );

        Optional<MovieMetadata> moveMetadataOptional = sut.getById(EXTERNAL_MOVE_ID);

        assertThat(moveMetadataOptional)
                .get()
                .extracting("title", "year")
                .contains("The Fast and the Furious", 2001);
    }

    @Test
    public void should_retry_if_failure_and_succeed_after_second_time() {
        FakeOmdbSimpleClient omdbSimpleClient = new FakeOmdbSimpleClient(2);

        OmdbExternalMovieMetadataProvider sut = new OmdbExternalMovieMetadataProvider(
                omdbSimpleClient,
                Duration.ofMillis(10),
                NOT_REQUIRED
        );

        Optional<MovieMetadata> moveMetadataOptional = sut.getById(EXTERNAL_MOVE_ID);

        assertThat(omdbSimpleClient.counter).isEqualTo(2);
        assertThat(moveMetadataOptional)
                .get()
                .extracting("title", "year")
                .contains("The Fast and the Furious", 2001);
    }

    @Test
    public void should_throw_exception_if_too_many_attempts() {
        FakeOmdbSimpleClient omdbSimpleClient = new FakeOmdbSimpleClient(10);

        OmdbExternalMovieMetadataProvider sut = new OmdbExternalMovieMetadataProvider(
                omdbSimpleClient,
                Duration.ofMillis(10),
                NOT_REQUIRED
        );

        assertThatThrownBy(() -> {
            sut.getById(EXTERNAL_MOVE_ID);
        }).isInstanceOf(ExternalServiceNotAvailableException.class);
    }

    private static class FakeOmdbSimpleClient implements OmdbSimpleClient {

        private final int successAfter;
        private int counter = 0;

        public FakeOmdbSimpleClient(int successAfter) {
            this.successAfter = successAfter;
        }

        @Override
        public SimplifiedResponse get(String url) {
            counter++;

            if (counter >= successAfter) {
                return new SimplifiedResponse(200, "{\"Title\": \"The Fast and the Furious\", \"Year\": 2001}");
            }

            throw new RuntimeException("Failed request");
        }
    }

}