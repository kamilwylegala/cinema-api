package pl.kwylegala.cinemaapi.framework.repository;

import javax.inject.Inject;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootTest
class DbConnectionTest {

    @Inject
    JdbcTemplate sut;

    @Test
    public void should_connect_to_db_and_get_result_of_select_query() {
        String s = sut.queryForObject("select 11", (row, num) -> {
            return row.getString(1);
        });

        Assertions.assertThat(s).isEqualTo("11");
    }

}