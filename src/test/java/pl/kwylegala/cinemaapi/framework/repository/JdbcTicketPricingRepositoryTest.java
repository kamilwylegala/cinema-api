package pl.kwylegala.cinemaapi.framework.repository;

import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing;
import pl.kwylegala.cinemaapi.app.management.pricing.TicketPricing.PricingType;
import pl.kwylegala.cinemaapi.app.movies.MovieId;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JdbcTicketPricingRepositoryTest {

    @Autowired
    JdbcTicketPricingRepository sut;

    @Test
    void should_insert_value() {
        sut.save(new TicketPricing(new MovieId(1), 20, PricingType.STANDARD));
    }

}