package pl.kwylegala.cinemaapi.framework.repository;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.ratings.MovieRatingByUser;
import pl.kwylegala.cinemaapi.app.ratings.MovieRatingRepository;
import pl.kwylegala.cinemaapi.app.ratings.Rating;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootTest
class JdbcMovieRatingRepositoryTest {

    @Autowired
    private MovieRatingRepository sut;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterEach
    void tearDown() {
        jdbcTemplate.execute("DELETE FROM ratings;");
    }

    @Test
    void should_get_average_rating_for_movie() {
        saveRatingForMovie(1, "customer1", 4);
        saveRatingForMovie(1, "customer2", 5);
        saveRatingForMovie(1, "admin", 5);

        Assertions.assertThat(sut.getAverageRatingForMovie(new MovieId(1)).getAverage()).isEqualTo(4.6667d);
    }

    private void saveRatingForMovie(int movieId, String amdin, int i) {
        sut.saveRating(
                new MovieRatingByUser(
                        new MovieId(movieId),
                        amdin,
                        new Rating(i)
                )
        );
    }
}