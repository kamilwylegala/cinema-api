package pl.kwylegala.cinemaapi.app.showtime;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryMovieShowTimeRepository implements MovieShowTimeRepository {

    private int autoIncrementedId = 0;

    private final List<MovieShowTime> storage = new ArrayList<>();

    public List<MovieShowTime> getStorage() {
        return storage;
    }

    @Override
    public void saveMovieShowTime(NewMovieShowTime movieShowTime) {
        storage.add(
                new MovieShowTime(
                        ++autoIncrementedId,
                        movieShowTime.getMovieId(),
                        movieShowTime.getRoom(),
                        movieShowTime.getShowTime()
                )
        );
    }

    @Override
    public List<MovieShowTime> findAll() {
        return storage.stream()
                .sorted(comparing(MovieShowTime::getShowTime))
                .collect(toList());
    }

    @Override
    public List<MovieShowTime> findAllByRange(Instant start, Instant end) {
        return storage.stream()
                .sorted(comparing(MovieShowTime::getShowTime))
                .filter(mst -> mst.getShowTime().compareTo(start) >= 0 && mst.getShowTime().compareTo(end) <= 0)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(int showTimeId) {
        storage.removeIf(movieShowTime -> movieShowTime.getId() == showTimeId);
    }

    @Override
    public boolean showTimeExists(int showTimeId) {
        return false;
    }

}
