package pl.kwylegala.cinemaapi.app.reporting.schedule;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.reporting.schedule.DaySchedule.ScheduledMovie;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;
import pl.kwylegala.cinemaapi.app.showtime.InMemoryMovieShowTimeRepository;
import pl.kwylegala.cinemaapi.app.showtime.NewMovieShowTime;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ScheduleReporterTest {

    private InMemoryMovieShowTimeRepository inMemoryMovieShowTimeRepository;
    private ScheduleReporter sut;

    private static Instant getInstant(String s) {
        return Instant.from(LocalDateTime.parse(s).atZone(ZoneId.of("UTC")));
    }

    @BeforeEach
    void setUp() {
        inMemoryMovieShowTimeRepository = new InMemoryMovieShowTimeRepository();
        sut = new ScheduleReporter(inMemoryMovieShowTimeRepository);
    }

    @Test
    void should_return_all_movie_times_for_given_date() {
        inMemoryMovieShowTimeRepository.saveMovieShowTime(
                new NewMovieShowTime(
                        new MovieId(1),
                        CinemaRoom.CINEMA_ROOM_1,
                        getInstant("2021-11-13T11:00:00")
                )
        );
        inMemoryMovieShowTimeRepository.saveMovieShowTime(
                new NewMovieShowTime(
                        new MovieId(2),
                        CinemaRoom.CINEMA_ROOM_2,
                        getInstant("2021-11-13T00:00:00")
                )
        );
        inMemoryMovieShowTimeRepository.saveMovieShowTime(
                new NewMovieShowTime(
                        new MovieId(3),
                        CinemaRoom.CINEMA_ROOM_2,
                        getInstant("2021-11-14T00:00:00")
                )
        );
        inMemoryMovieShowTimeRepository.saveMovieShowTime(
                new NewMovieShowTime(
                        new MovieId(4),
                        CinemaRoom.CINEMA_ROOM_2,
                        getInstant("2021-11-12T23:59:59")
                )
        );
        inMemoryMovieShowTimeRepository.saveMovieShowTime(
                new NewMovieShowTime(
                        new MovieId(5),
                        CinemaRoom.CINEMA_ROOM_1,
                        getInstant("2021-11-13T23:59:59")
                )
        );

        DaySchedule daySchedule = sut.getScheduleFor(LocalDate.of(2021, 11, 13));

        Assertions.assertThat(daySchedule.getScheduledMovies())
                .extracting(ScheduledMovie::getMovieId)
                .contains(new MovieId(1), new MovieId(2), new MovieId(5));
    }
}