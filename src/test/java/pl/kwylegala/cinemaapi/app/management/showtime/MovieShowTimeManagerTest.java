package pl.kwylegala.cinemaapi.app.management.showtime;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.time.Instant;

import pl.kwylegala.cinemaapi.app.movies.MovieId;
import pl.kwylegala.cinemaapi.app.movies.MovieNotFoundException;
import pl.kwylegala.cinemaapi.app.movies.MovieRepository;
import pl.kwylegala.cinemaapi.app.showtime.CinemaRoom;
import pl.kwylegala.cinemaapi.app.showtime.InMemoryMovieShowTimeRepository;
import pl.kwylegala.cinemaapi.app.showtime.MovieShowTime;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MovieShowTimeManagerTest {

    private static final Instant BASE_TIME = Instant.ofEpochSecond(1636493834);

    private MovieShowTimeManager sut;
    private InMemoryMovieShowTimeRepository inMemoryMovieShowTimeRepository;
    private MovieRepository movieRepository;

    @BeforeEach
    void setUp() {
        movieRepository = mock(MovieRepository.class);
        when(movieRepository.exists(any())).thenReturn(true);

        inMemoryMovieShowTimeRepository = new InMemoryMovieShowTimeRepository();

        sut = new MovieShowTimeManager(inMemoryMovieShowTimeRepository, movieRepository);
    }

    @Test
    void should_throw_exception_if_movie_to_schedule_is_not_supported() {
        when(movieRepository.exists(new MovieId(1))).thenReturn(false);

        assertThatThrownBy(
                () -> {
                    sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));
                }
        ).isInstanceOf(MovieNotFoundException.class);
    }

    @Test
    public void should_save_new_show_time() {
        sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));

        Assertions.assertThat(inMemoryMovieShowTimeRepository.getStorage())
                .extracting(MovieShowTime::getMovieId)
                .contains(new MovieId(1));
    }

    @Test
    public void should_reject_saving_new_movie_show_time_if_it_starts_during_other_movie_show_in_the_same_room() {
        sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));

        assertConflictForNewMovieShowTimeFor(BASE_TIME.plus(Duration.ofMinutes(60)));
    }

    @Test
    public void should_reject_saving_new_movie_to_the_same_hour_of_existing_movie_in_the_same_room() {
        sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));

        assertConflictForNewMovieShowTimeFor(BASE_TIME);
    }

    @Test
    public void should_reject_saving_new_movie_show_time_if_it_ends_during_other_movie_show_in_the_same_room() {
        sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));

        assertConflictForNewMovieShowTimeFor(BASE_TIME.minus(Duration.ofMinutes(60)));
    }

    @Test
    public void should_save_new_show_time_if_movie_starts_during_other_movie_show_time_but_in_different_room() {
        sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));
        sut.scheduleMovie(
                new MovieScheduleCommand(
                        new MovieId(2),
                        CinemaRoom.CINEMA_ROOM_2,
                        BASE_TIME.plus(Duration.ofMinutes(30))
                )
        );

        Assertions.assertThat(inMemoryMovieShowTimeRepository.getStorage())
                .extracting(MovieShowTime::getMovieId)
                .contains(new MovieId(1), new MovieId(2));
    }

    @Test
    public void should_save_new_show_time_if_movie_ends_during_other_movie_show_time_but_in_different_room() {
        sut.scheduleMovie(new MovieScheduleCommand(new MovieId(1), CinemaRoom.CINEMA_ROOM_1, BASE_TIME));
        sut.scheduleMovie(
                new MovieScheduleCommand(
                        new MovieId(2),
                        CinemaRoom.CINEMA_ROOM_2,
                        BASE_TIME.minus(Duration.ofMinutes(30))
                )
        );

        Assertions.assertThat(inMemoryMovieShowTimeRepository.getStorage())
                .extracting(MovieShowTime::getMovieId)
                .contains(new MovieId(1), new MovieId(2));
    }

    private void assertConflictForNewMovieShowTimeFor(Instant showTime) {
        assertThatThrownBy(() -> {
            sut.scheduleMovie(new MovieScheduleCommand(
                    new MovieId(2),
                    CinemaRoom.CINEMA_ROOM_1,
                    showTime
            ));
        }).isInstanceOf(MovieShowTimeConflictException.class);
    }
}